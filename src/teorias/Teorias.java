/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teorias;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static jdk.nashorn.tools.ShellFunctions.input;

/**
 *
 * @author Yisus
 */
public class Teorias {

    /**
     * @param args the command line arguments
     */
    
    private static int score;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String asd;
        System.out.println("Ingrese cadena");
        
        
        asd = sc.next();
        Met1(asd);
        Star(asd);
        End(asd);
        mult3(asd);
        
        if (score == 4) {
            System.out.println("Cadena ingresa SI pertenece al lenguje  TD {0-9}, cumpliendo con las restrincciones");
        }

    }

    
    
    private static void Met1(String cadena) {

      // Asegura que solo pueda ingresar de 0 a 9
        Pattern pat = Pattern.compile("[^A-Za-z.@_-~#]+");
     Matcher mat = pat.matcher(cadena);
     if (mat.matches()) {
         score = score+1;
     } else {
         System.out.println("No Válido");
            System.exit(0);
     }
    }

    
    // Asegura que comience con 00 o 46 (primeros digitos de mi ID)
    private static void Star(String cadena) {

        Pattern pat = Pattern.compile("^(46|00).*");
        Matcher mat = pat.matcher(cadena);
        if (mat.matches()) {
            score = score+1;
        } else {
            System.out.println("No Válido");
            System.exit(0);
        }

     
    }

    //Asegura que no termine en la subcadena 37 o 39
    private static String End(String cadena) {
        Pattern pat = Pattern.compile(".*[^37|39]$");
        Matcher mat = pat.matcher(cadena);
        if (mat.matches()) {
            System.out.println("El lenguaje ingresado no es valido");
        } else {
            score = score+1;
        }
        return null;
    }

    
       //Primero determina si tiene el 3. Si lo tiene recorre el string contando la cantidad de 3´s
        // si la cantidad de 3 son multiplo de 3 sera validada la cadena
    private static void mult3(String cadena) {

        int count = 0;
        char car;
        char comp = '3';
        String s = null;

        Pattern pat = Pattern.compile(".*3.*");
        Matcher mat = pat.matcher(cadena);
        if (mat.matches()) {

            for (int x = 0; x < cadena.length(); x++) {
                car = cadena.charAt(x);
                if (car == comp) {
                    count = count + 1;
                }

            }

            if (count % 3 == 0) {
                score = score+1;
            } else {
                System.out.println("la cadena NO pertenece al lenguaje");
            }

        } else {
            score = score+1;
        }


    }

}
